package com.vvrSolar;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created with IntelliJ IDEA.
 * User: v.vasilatos
 * Date: 2/11/2012
 * Time: 3:23 μμ
 * To change this template use File | Settings | File Templates.
 */
public class Counter extends Activity implements View.OnClickListener {

    Spinner spinnerRegion;
    Spinner spinnerTypeOfRoof;
    Spinner spinnerOrientation;
    EditText etextSquare;
    EditText etextPrice;
    TextView textGain;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.counter);


        etextSquare = (EditText)findViewById(R.id.inputSquare);
        etextSquare.setInputType(0x00002002);
        etextPrice = (EditText)findViewById(R.id.inputPrice);
        etextPrice.setInputType(0x00002002);
        textGain = (TextView)findViewById(R.id.labelGain);
        addListenerOnSpinnerItemSelectionRegion();
        addListenerOnSpinnerItemSelectionTypeOfRoof();
        addListenerOnSpinnerItemSelectionOrientation();

        Button btnCountGain=(Button) findViewById(R.id.buttonCountGain);
        btnCountGain.setOnClickListener(this);


    }

    public void addListenerOnSpinnerItemSelectionOrientation() {
        spinnerOrientation = (Spinner) findViewById(R.id.spinnerOrientation);
        spinnerOrientation.setOnItemSelectedListener(new CustomOnItemSelectedListenerOrientation());
    }

    public void addListenerOnSpinnerItemSelectionRegion() {
        spinnerRegion = (Spinner) findViewById(R.id.spinnerRegion);
        spinnerRegion.setOnItemSelectedListener(new CustomOnItemSelectedListenerRegion());
    }

    public void addListenerOnSpinnerItemSelectionTypeOfRoof() {
        spinnerTypeOfRoof = (Spinner) findViewById(R.id.spinnerRoof);
        spinnerTypeOfRoof.setOnItemSelectedListener(new CustomOnItemSelectedListenerTypeOfRoof());
    }

    public void onClick(View v) {
        try{
            String strRegion = String.valueOf(spinnerRegion.getSelectedItem());
            String strTypeOfRoof = String.valueOf(spinnerTypeOfRoof.getSelectedItem());
            String strOrientation = String.valueOf(spinnerOrientation.getSelectedItem());
            double doubleSquare = Double.valueOf(etextSquare.getText().toString());
            double doublePrice = Double.valueOf(etextPrice.getText().toString());
            double factor;
            double regionFactor = 0;
            double orientationFactor;
            String fac;
            if(strTypeOfRoof.contentEquals("Επίπεδη")){
                factor = 12;
            }else{
                factor = 6.5;
            }

            if(strRegion.contentEquals("Αιτωλοακαρνανία")){
                regionFactor = 1380;
            }
            else if (strRegion.contentEquals("Άργος")){
                regionFactor = 1440;
            }
            else if (strRegion.contentEquals("Αρκαδία")){
                regionFactor = 1410;
            }
            else if (strRegion.contentEquals("Άρτα")){
                regionFactor = 1430;
            }
            else if (strRegion.contentEquals("Αττική")){
                regionFactor = 1470;
            }
            else if (strRegion.contentEquals("Βοιωτία")){
                regionFactor = 1390;
            }
            else if (strRegion.contentEquals("Δράμα")){
                regionFactor = 1340;
            }
            else if (strRegion.contentEquals("Ευριτανία")){
                regionFactor = 1240;
            }
            else if (strRegion.contentEquals("Έυβοια")){
                regionFactor = 1420;
            }
            else if (strRegion.contentEquals("Φλώρινα")){
                regionFactor = 1220;
            }
            else if (strRegion.contentEquals("Φωκίδα")){
                regionFactor = 1470;
            }
            else if (strRegion.contentEquals("Γρεβενά")){
                regionFactor = 1300;
            }
            else if (strRegion.contentEquals("Ημαθία")){
                regionFactor = 1320;
            }
            else if (strRegion.contentEquals("Ηράκλειο")){
                regionFactor = 1520;
            }
            else if (strRegion.contentEquals("Ιωάννινα")){
                regionFactor = 1340;
            }
            else if (strRegion.contentEquals("Καρδίτσα")){
                regionFactor = 1350;
            }
            else if (strRegion.contentEquals("Καστοριά")){
                regionFactor = 1280;
            }
            else if (strRegion.contentEquals("Καβάλα")){
                regionFactor = 1360;
            }
            else if (strRegion.contentEquals("Κεφαλλονιά\n")){
                regionFactor = 1380;
            }
            else if (strRegion.contentEquals("Κέρκυρα")){
                regionFactor = 1950;
            }
            else if (strRegion.contentEquals("Κιλκίς")){
                regionFactor = 1340;
            }
            else if (strRegion.contentEquals("Κορινθία")){
                regionFactor = 1440;
            }
            else if (strRegion.contentEquals("Κοζάνη")){
                regionFactor = 1300;
            }
            else if (strRegion.contentEquals("Κυκλάδες")){
                regionFactor = 1510;
            }
            else if (strRegion.contentEquals("Λακωνία")){
                regionFactor = 1460;
            }
            else if (strRegion.contentEquals("Λαμία")){
                regionFactor = 1320;
            }
            else if (strRegion.contentEquals("Λάρισα")){
                regionFactor = 1380;
            }
            else if (strRegion.contentEquals("Λασίθι")){
                regionFactor = 1580;
            }
            else if (strRegion.contentEquals("Λευκάδα")){
                regionFactor = 1390;
            }
            else if (strRegion.contentEquals("Λέσβος")){
                regionFactor = 1400;
            }
            else if (strRegion.contentEquals("Μαγνησία")){
                regionFactor = 1380;
            }
            else if (strRegion.contentEquals("Μεσσηνία")){
                regionFactor = 1450;
            }
            else if (strRegion.contentEquals("Πάτρα")){
                regionFactor = 1460;
            }
            else if (strRegion.contentEquals("Πέλλα")){
                regionFactor = 1300;
            }
            else if (strRegion.contentEquals("Πιερία")){
                regionFactor = 1240;
            }
            else if (strRegion.contentEquals("Πρέβεζα")){
                regionFactor = 1390;
            }
            else if (strRegion.contentEquals("Πύργος")){
                regionFactor = 1530;
            }
            else if (strRegion.contentEquals("Ρέθυμνο")){
                regionFactor = 1510;
            }
            else if (strRegion.contentEquals("Ρόδος")){
                regionFactor = 1500;
            }
            else if (strRegion.contentEquals("Σάμος")){
                regionFactor = 1480;
            }
            else if (strRegion.contentEquals("Σέρρες")){
                regionFactor = 1330;
            }
            else if (strRegion.contentEquals("Θεσπρωτία")){
                regionFactor = 1380;
            }
            else if (strRegion.contentEquals("Θεσσαλονίκη")){
                regionFactor = 1370;
            }
            else if (strRegion.contentEquals("Τρίκαλα")){
                regionFactor = 1330;
            }
            else if (strRegion.contentEquals("Χαλκιδική")){
                regionFactor = 1360;
            }
            else if (strRegion.contentEquals("Χανιά")){
                regionFactor = 1520;
            }
            else if (strRegion.contentEquals("Χίος")){
                regionFactor = 1490;
            }
            else if (strRegion.contentEquals("Ζάκυνθος")){
                regionFactor = 1470;
            }
            else if (strRegion.contentEquals("Έβρος")){
                regionFactor = 1350;
            }
            else if (strRegion.contentEquals("Ξάνθη")){
                regionFactor = 1330;
            }
            else if (strRegion.contentEquals("Ροδόπη")){
                regionFactor = 1370;
            }


            double kw = doubleSquare / factor;
            if (kw>10){
                kw=10;
            }

            if(strTypeOfRoof.contentEquals("Επίπεδη")){
                orientationFactor = 0.9;
            }else{
                if(strOrientation.contentEquals("Ν")){
                    orientationFactor = 1;
                }
                else if(strOrientation.contentEquals("ΝΔ,ΝΑ")){
                    orientationFactor = 0.95;
                }
                else if(strOrientation.contentEquals("Α-Δ")){
                    orientationFactor = 0.85;
                }
                else if(strOrientation.contentEquals("ΒΔ,ΒΑ")){
                    orientationFactor = 0.95;
                }
                else if(strOrientation.contentEquals("Β")){
                    orientationFactor = 0.6;
                }
                else
                    orientationFactor = 0;
            }

            double yearlyKW = regionFactor * kw;
            double gain =  yearlyKW * orientationFactor * doublePrice;

            BigDecimal bd = new BigDecimal(Double.toString(gain));
            bd.setScale(2, RoundingMode.HALF_EVEN);
            textGain.setText(bd.toString() + "€ ετησίως" );
            textGain.setTextSize(32);
        }catch (Exception e){
            Toast.makeText(this, "Παρακαλώ συμπληρώστε όλα τα πεδία", Toast.LENGTH_SHORT).show();
        }

    }

}
