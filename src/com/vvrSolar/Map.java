package com.vvrSolar;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.google.android.maps.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: v.vasilatos
 * Date: 5/11/2012
 * Time: 12:59 μμ
 * To change this template use File | Settings | File Templates.
 */
public class Map extends MapActivity {

    @Override
    protected boolean isRouteDisplayed() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);

        MapView mapView = (MapView) findViewById(R.id.mapview);
        mapView.setBuiltInZoomControls(true);

        List<Overlay> mapOverlays = mapView.getOverlays();
        Drawable drawable = this.getResources().getDrawable(R.drawable.androidmarker);
        MapItemizedOverlay itemizedoverlay = new MapItemizedOverlay(drawable, this);

        GeoPoint point = new GeoPoint(37979116,23674033);
        OverlayItem overlayitem = new OverlayItem(point, "Επισκευθείτε μας", "Τμήμα ΗΥΣ\nΠέτρου Ράλλη και Θηβών 250\nτηλ:xxxxxxxxxx");

        itemizedoverlay.addOverlay(overlayitem);
        mapOverlays.add(itemizedoverlay);

        GeoPoint pointAnnex = new GeoPoint(37941761,23652703);
        OverlayItem overlayitemAnnex = new OverlayItem(pointAnnex, "Παράρτημα", "ΠΑ.ΠΕΙ.\nΜ. Καραολή και Α. Δημητρίου 80, Πειραιάς,\nτηλ:xxxxxxxxxx");

        itemizedoverlay.addOverlay(overlayitemAnnex);
        mapOverlays.add(itemizedoverlay);


    }
}
