package com.vvrSolar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final Button btnCounter=(Button) findViewById(R.id.buttonCounter);
        btnCounter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent("countyoursolarenergy.COUNTER"));
            }
        });

        final Button btnDocuments=(Button) findViewById(R.id.buttonDocuments);
                        btnDocuments.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                startActivity(new Intent("countyoursolarenergy.DOCUMENTS"));
                            }
                        });

        final Button btnMap=(Button) findViewById(R.id.buttonMap);
                btnMap.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        startActivity(new Intent("countyoursolarenergy.MAP"));
                    }
                });

        final Button btnContact=(Button) findViewById(R.id.buttonContact);
                        btnContact.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                startActivity(new Intent("countyoursolarenergy.CONTACT"));
                            }
                        });

        final Button exit=(Button) findViewById(R.id.buttonExit);
        exit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                finish();
                System.exit(0);
            }
        });

    }
}
