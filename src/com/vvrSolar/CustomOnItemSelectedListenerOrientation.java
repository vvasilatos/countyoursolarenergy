package com.vvrSolar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: v.vasilatos
 * Date: 8/11/2012
 * Time: 12:12 μμ
 * To change this template use File | Settings | File Templates.
 */
public class CustomOnItemSelectedListenerOrientation implements AdapterView.OnItemSelectedListener {

    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
            Toast.makeText(parent.getContext(),
                    "Προσανατολισμός που διαλέξατε : " + parent.getItemAtPosition(pos).toString(), Toast.LENGTH_SHORT).show();
        }


        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }
}
