package com.vvrSolar;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: v.vasilatos
 * Date: 5/11/2012
 * Time: 3:33 μμ
 * To change this template use File | Settings | File Templates.
 */
public class Documents extends ListActivity {

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        String[] values = new String[] { "Κάτοψη στέγης", "Αντίγραφο πρόσφατου λογαριασμού κατανάλωσης ηλεκτρικού ρεύματος", "Οικοδομική άδεια",
                "Τοπογραφικό της θέσης εγκατάστασης (προκειμένου για κτιριακές εγκαταστάσεις εκτός σχεδίου πόλεως",
                "Τίτλος κυριότητας (αντίγραφο συμβολαιογραφικής πράξης και πιστοποιητικού μεταγραφής της στουποθηκοφυλακείο",
                "Αντίγραφο ταυτότητας" ,
                "Αριθμός φορολογικού μητρώου (Α.Φ.Μ.)" ,
                "Εκκαθαριστικός εφορίας δύο τελευταίων ετών" ,
                "Βεβαίωση μηνιαίων αποδοχών. (Για μισθωτούς)" ,
                "Δήλωση Ε9" };
        // Use your own layout
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.documents, R.id.label, values);
        setListAdapter(adapter);

        /* final Button btnShareEvents=(Button) findViewById(R.id.btnShareEvents);
        btnShareEvents.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Κάτοψη στέγης, Αντίγραφο πρόσφατου λογαριασμού κατανάλωσης ηλεκτρικού ρεύματος Οικοδομική άδεια, Τοπογραφικό της θέσης εγκατάστασης (προκειμένου για κτιριακές εγκαταστάσεις εκτός σχεδίου πόλεως, Τίτλος κυριότητας (αντίγραφο συμβολαιογραφικής πράξης και πιστοποιητικού μεταγραφής της στουποθηκοφυλακείο, Αντίγραφο ταυτότητας, Αριθμός φορολογικού μητρώου (Α.Φ.Μ.), Εκκαθαριστικός εφορίας δύο τελευταίων ετών, Βεβαίωση μηνιαίων αποδοχών. (Για μισθωτούς), Δήλωση Ε9");
                startActivity(Intent.createChooser(shareIntent, "Share via"));
            }
        });*/



    }

    // Initiating Menu XML file (menu.xml)
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.documentsmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.menu_bookmark:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message
                Toast.makeText(Documents.this, "Bookmark is Selected", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.menu_save:
                Toast.makeText(Documents.this, "Save is Selected", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.menu_search:
                Toast.makeText(Documents.this, "Search is Selected", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.menu_share:
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Κάτοψη στέγης, Αντίγραφο πρόσφατου λογαριασμού κατανάλωσης ηλεκτρικού ρεύματος Οικοδομική άδεια, Τοπογραφικό της θέσης εγκατάστασης (προκειμένου για κτιριακές εγκαταστάσεις εκτός σχεδίου πόλεως, Τίτλος κυριότητας (αντίγραφο συμβολαιογραφικής πράξης και πιστοποιητικού μεταγραφής της στουποθηκοφυλακείο, Αντίγραφο ταυτότητας, Αριθμός φορολογικού μητρώου (Α.Φ.Μ.), Εκκαθαριστικός εφορίας δύο τελευταίων ετών, Βεβαίωση μηνιαίων αποδοχών. (Για μισθωτούς), Δήλωση Ε9");
                startActivity(Intent.createChooser(shareIntent, "Share via"));
                return true;

            case R.id.menu_delete:
                Toast.makeText(Documents.this, "Delete is Selected", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.menu_preferences:
                Toast.makeText(Documents.this, "Preferences is Selected", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
